package com.reservation.elibrary.entities;


import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String email;
    private ArrayList<String> roles;
    private String password;
    @OneToMany(mappedBy = "user")
    private ArrayList<Reservation> reservations;
}
