package com.reservation.elibrary.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Data
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date dateDebut;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date dateFin;
    private String etat;
    @ManyToOne
    private Book book;
    @ManyToOne
    private User user;

}
