package com.reservation.elibrary.controllers;

import com.reservation.elibrary.entities.Book;
import com.reservation.elibrary.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/all")
    public String allBooks(Model model) {
        List<Book> books = bookService.getAllBooks();
        model.addAttribute("books", books);
        return "book/all";
    }

    @GetMapping("/add")
    public  String add(Model model) {
        Book book = new Book();
        model.addAttribute("book", book);
        return  "book/add";
    }

    @PostMapping("/save")
    public  String save(@ModelAttribute("book") Book book) {
        bookService.saveBook(book);
        return "redirect:/book/all";
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id) {
        bookService.deleteBook(id);
        return "redirect:/book/all";
    };

    @GetMapping("/edit/{id}")
    public  String edit(@PathVariable("id") int id, Model model) {
        Book book = bookService.getBookById(id);
        model.addAttribute("book", book);
        return "book/update";
    }

    @PostMapping("/update/")
    public String update(@Validated Book book, BindingResult bindingResult) {
        if (bindingResult.hasErrors() ) {
            return "redirect:/book/edit/"+book.getId();
        }
        bookService.saveBook(book);
        return "redirect:/book/all";
    }
}
