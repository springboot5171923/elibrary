package com.reservation.elibrary.controllers;

import com.reservation.elibrary.entities.Book;
import com.reservation.elibrary.entities.Reservation;
import com.reservation.elibrary.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @GetMapping("/all")
    public String allReservations (Model model) {
        List<Reservation> allReservations = reservationService.getAllReservations();
        model.addAttribute("allReservations", allReservations);
        return "allReservations";

    }
}
