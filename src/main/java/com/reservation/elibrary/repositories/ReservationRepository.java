package com.reservation.elibrary.repositories;

import com.reservation.elibrary.entities.Book;
import com.reservation.elibrary.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
}
